# Buffy

A simple shared buffer in a web page.

## Installation

Clone the repository and run `docker build` to create the image.

## Usage

Run the image with `docker run -p 8080:8080 -p 3000:3000 <image>`. The web page will be available at `http://localhost:3000`.

The port 8080 is used for the websocket connection, and the port 3000 is used for the web page.

An Sqlite database is used to store the buffer content. The database file is stored under `/app/database.db`.

Environment variables can be used to configure the application:

| Variable | Description | Default value |
| -------- | ----------- | ------------- |
| `WS_PORT` | Port exposed for the websocket connection | `8080` |
| `WS_PATH` | Path for the websocket connection | `""` |
