import { WebSocketServer } from "ws";
import Database from "better-sqlite3";
import express from "express";
import cors from "cors";

const CONFIG = {
  WS_PORT: +(process.env.WS_PORT || 8080),
  WS_PATH: process.env.WS_PATH || "",
};

const app = express();

app.use(
  cors({
    origin: "*",
  })
);

app.use(express.static("public"));

app.get("/config", (_, res) => {
  res.json(CONFIG);
});

app.listen(process.env.PORT || 3000, () => {
  console.log(`Server listening on port ${process.env.PORT || 3000}`);
});

const db = new Database("database.db");
db.pragma("journal_mode = WAL");

const schema = `CREATE TABLE IF NOT EXISTS buffers (
    timestamp DATETIME DEFAULT CURRENT_TIMESTAMP,
    content TEXT
);`;

db.exec(schema);

const wss = new WebSocketServer({
  port: 8080,
  path: CONFIG.WS_PATH,
});

wss.on("connection", (ws) => {
  const select = db.prepare(
    "SELECT content FROM buffers ORDER BY timestamp DESC LIMIT 1"
  );
  const buffer = select.get() as { content: string };
  ws.send(JSON.stringify({ buffer: buffer?.content || "" }));

  ws.on("error", console.error);

  ws.on("message", (data: string) => {
    const parsedData = JSON.parse(data) as { buffer: string };
    const insert = db.prepare("INSERT INTO buffers (content) VALUES (?)");
    insert.run(parsedData.buffer);
    wss.clients.forEach((client) => {
      client.send(JSON.stringify({ buffer: parsedData.buffer }));
    });
  });

  wss.clients.forEach((client) => {
    client.send(JSON.stringify({ users: wss.clients.size }));
  });

  ws.on("close", () => {
    wss.clients.forEach((client) => {
      client.send(JSON.stringify({ users: wss.clients.size }));
    });
  });
});

wss.on("disconnect", () => {
  wss.clients.forEach((client) => {
    client.send(JSON.stringify({ users: wss.clients.size }));
  });
});

wss.on("error", console.error);
