FROM node:lts-alpine as front-builder
WORKDIR /app
COPY ./client/package.json ./
RUN npm install
COPY ./client ./
RUN npm run build

FROM node:lts-alpine as back-builder
WORKDIR /app
COPY ./server/package.json ./
RUN npm install
COPY ./server ./
RUN npm run build

FROM node:lts-alpine
WORKDIR /app
COPY --from=back-builder /app/dist ./dist
COPY --from=back-builder /app/package*.json ./
COPY --from=front-builder /app/dist ./public
RUN npm ci --omit=dev
CMD ["npm", "run", "prod"]
