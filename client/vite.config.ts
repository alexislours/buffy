import { defineConfig } from "vite";

export default defineConfig({
  build: {
    assetsInlineLimit: 1000000,
    target: "esnext",
    modulePreload: {
      polyfill: false,
    },
  },
});
