import "./style.css";
import { setConnectionStatus } from "./utils/dom";
import { setupEvents } from "./utils/events";
import { setupSockets } from "./utils/sockets";
import { isDarkMode, setDarkMode } from "./utils/theme";

setDarkMode(isDarkMode());

fetch(import.meta.env.DEV ? "http://localhost:3000/config" : "./config").then(
  async (res) => {
    const config = (await res.json()) as { WS_PORT: number; WS_PATH: string };
    setupSockets(config);
    setConnectionStatus("disconnected");
    setupEvents();
  }
);
