import { copyAnimation } from "./dom";


const buffer = document.querySelector("#buffer") as HTMLTextAreaElement;
const copyButton =
  document.querySelector("#copy-button") as HTMLButtonElement;
const textStyleButton =
  document.querySelector("#text-style-button") as HTMLButtonElement;
const monoSvg = document.querySelector("#font-mono") as SVGElement;
const sansSvg = document.querySelector("#font-default") as SVGElement;

export const setupEvents = () => {
  if (textStyleButton && buffer && monoSvg && sansSvg) {
    setFontIcon();
    textStyleButton.addEventListener("click", fontStyleToggleListener);
  }

  if (buffer && copyButton) {
    copyButton.addEventListener("click", copyListener);
  }
};

const setFontIcon = () => {
  if (buffer.classList.contains("font-mono") || localStorage.getItem("font-mono") === "true") {
    buffer.classList.add("font-mono");
    sansSvg.classList.remove("hidden");
    monoSvg.classList.add("hidden");
  }
}

const fontStyleToggleListener = () => {
  const isMono = buffer.classList.contains("font-mono") || localStorage.getItem("font-mono") === "true";
  if (isMono) {
    buffer.classList.remove("font-mono");
    sansSvg.classList.add("hidden");
    monoSvg.classList.remove("hidden");
    localStorage.removeItem("font-mono");
  } else {
    buffer.classList.add("font-mono");
    sansSvg.classList.remove("hidden");
    monoSvg.classList.add("hidden");
    localStorage.setItem("font-mono", "true");
  }
}

const copyListener = () => {
  if (!navigator.clipboard) {
    fallbackCopyTextToClipboard(buffer.value);
    return;
  }
  navigator.clipboard.writeText(buffer.value).then(
    () => {
      copyAnimation();
      return;
    },
    (err) => {
      console.error("Async: Could not copy text: ", err);
    }
  );
}

const fallbackCopyTextToClipboard = (text: string) => {
  const textArea = document.createElement("textarea");
  textArea.value = text;
  textArea.style.top = "0";
  textArea.style.left = "0";
  textArea.style.position = "fixed";

  document.body.appendChild(textArea);
  textArea.focus();
  textArea.select();

  try {
    document.execCommand("copy");
    copyAnimation();
  } catch (err) {
    console.error("Still unable to copy", err);
  }

  document.body.removeChild(textArea);
};
