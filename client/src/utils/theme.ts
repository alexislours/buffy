const themeToggle = document.querySelector(
  "#theme-button"
) as HTMLButtonElement;
const moonIcon = document.querySelector("#moon") as SVGElement;
const sunIcon = document.querySelector("#sun") as SVGElement;

themeToggle.addEventListener("click", () => {
  toggleDarkMode();
});

const isDarkMode = () => {
  let darkMode = false;
  if ("theme" in localStorage) {
    darkMode = localStorage.theme === "dark";
  } else if (window.matchMedia("(prefers-color-scheme: dark)").matches) {
    darkMode = true;
  }

  return darkMode;
};

const setDarkMode = (darkMode: boolean, save = false) => {
  if (darkMode) {
    document.documentElement.classList.add("dark");
    moonIcon.classList.add("hidden");
    sunIcon.classList.remove("hidden");
    if (save) localStorage.theme = "dark";
  } else {
    document.documentElement.classList.remove("dark");
    moonIcon.classList.remove("hidden");
    sunIcon.classList.add("hidden");
    if (save) localStorage.theme = "light";
  }
  return darkMode;
};

const toggleDarkMode = () => {
  return setDarkMode(!isDarkMode(), true);
};

export { isDarkMode, setDarkMode, toggleDarkMode };
