import { setConnectedUsers, setConnectionStatus } from "./dom";
import { updateBuffer } from "./dom";

let timeout = -1;
let reconnectTimeout = -1;

type OpeningMessage = {
  users: number;
};

type BufferUpdateMessage = {
  buffer: string;
};

export const setupSockets = (config: { WS_PORT: number; WS_PATH: string }) => {
  const socket = new WebSocket(
    `ws://${window.location.hostname}:${config.WS_PORT || 8080}/${
      config.WS_PATH || ""
    }`
  );
  const buffer: HTMLTextAreaElement | null = document.querySelector("#buffer");

  socket.onopen = () => {
    setConnectionStatus("connected");
  };
  socket.onmessage = (event: MessageEvent<string>) => {
    const data = JSON.parse(event.data) as OpeningMessage | BufferUpdateMessage;
    if ("users" in data) {
      setConnectedUsers(data.users);
    } else if ("buffer" in data) {
      updateBuffer(data.buffer);
    }
  };
  if (buffer) {
    buffer.addEventListener("input", () => {
      clearTimeout(timeout);
      timeout = window.setTimeout(() => {
        socket.send(JSON.stringify({ buffer: buffer.value }));
      }, 500);
    });
  }

  socket.onclose = () => {
    setConnectionStatus("disconnected");
    clearTimeout(reconnectTimeout);
    reconnectTimeout = window.setTimeout(() => {
      setupSockets(config);
    }, 1000);
  };
};
