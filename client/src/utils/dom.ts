export const setConnectedUsers = (users: number) => {
  const connectedUsersElement = document.querySelector("#connected-users");
  if (connectedUsersElement) {
    connectedUsersElement.textContent = users.toString();
  }
};

export const setConnectionStatus = (status: "connected" | "disconnected") => {
  const connectionStatusElement = document.querySelector(
    "#connection-indicator"
  );
  if (connectionStatusElement) {
    switch (status) {
      case "connected":
        connectionStatusElement.classList.remove("disconnected");
        connectionStatusElement.classList.add("connected");
        connectionStatusElement.setAttribute(
          "title",
          "Connected to WebSocket server"
        );
        break;
      case "disconnected":
        connectionStatusElement.classList.remove("connected");
        connectionStatusElement.classList.add("disconnected");
        connectionStatusElement.setAttribute(
          "title",
          "Disconnected from WebSocket server"
        );
        break;
    }
  }
};

export const updateBuffer = (buffer: string) => {
  const bufferElement: HTMLTextAreaElement | null =
    document.querySelector("#buffer");
  if (bufferElement) {
    bufferElement.value = buffer;
  }
};

export const copyAnimation = async () => {
  const copyIcon = document.querySelector("#copy") as SVGElement;
  const checkIcon = document.querySelector("#copy-check") as SVGElement;

  const sleep = (ms: number) =>
    new Promise((resolve) => setTimeout(resolve, ms));

  copyIcon.classList.add("opacity-0");
  await sleep(100);
  copyIcon.classList.add("hidden");
  checkIcon.classList.remove("hidden");
  await sleep(1500);
  checkIcon.classList.add(
    "animate-jump-out",
    "animate-once",
    "animate-duration-[350ms]",
    "animate-ease-out"
  );
  await sleep(400);
  checkIcon.classList.remove(
    "animate-jump-out",
    "animate-once",
    "animate-duration-[350ms]",
    "animate-ease-out"
  );
  checkIcon.classList.add("hidden");
  copyIcon.classList.remove("hidden");
  await sleep(300);
  copyIcon.classList.remove("opacity-0");
};
